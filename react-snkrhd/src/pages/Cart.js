import ProductCard from '../components/ProductCard';
import Loading from '../components/Loading';
import { useEffect, useState, useContext } from 'react';
import {Container, Row, Col, Card, CardImg} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';

export default function Cart() {
	const navigate = useNavigate()
  const [cartContents, setCartContents] = useState([]);
  const [loading, setLoading] = useState(true)
  const [totalCheckoutPrice, setTotalCheckoutPrice] = useState(0)
  const { user } = useContext(UserContext);
	const getCartContents = () => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/cartContents/${user.id}`, {
		  	headers: {
	            Authorization: `Bearer ${user.jwt}`
	        }
		})
		.then(response => response.json())
		.then(result => {
			console.log(result, "result")

			setCartContents(result)
			if(result.length > 0) {
			 let total = 0;
			 result.forEach(item => total += item.totalPrice)
			 setTotalCheckoutPrice(total)
			}
			
			setLoading(false)
		})
	}

	useEffect(() => {
		getCartContents()
	}, [])


	const handleCheckout = () => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/checkOut`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.jwt}`
      },
    })
    .then(response => response.json())
    .then(result => {
      Swal.fire({
        title: 'Checkout Successful!',
        icon: 'success',
        text: result.message
      })
      navigate(-1)
    }).catch(error =>console.log(error, "Err"));
	}
  return (
  	(loading) ?
			
			<Loading/>
		:  
		 cartContents.length > 0 ? 
		 	(
		 		<>
			 		{cartContents.map(item => (
				 		<Card>
					      <Card.Body>
					      <Card.Subtitle>Name:</Card.Subtitle>
					        <Card.Text>{item?.name}</Card.Text>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{item?.description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>{item?.price}</Card.Text>
					        <Card.Subtitle>Quantity:</Card.Subtitle>
					        <Card.Text>{item?.quantity}</Card.Text>
					        <Card.Subtitle>Total Price:</Card.Subtitle>
					        <Card.Text>PHP {item?.totalPrice}</Card.Text>
					        
					      </Card.Body>
					    </Card>
				 	))}
			 		<div>
			 			<h2>Total Checkout Price:</h2>
			 			<h3>{totalCheckoutPrice}</h3>
			 			<button onClick={handleCheckout}>Checkout</button>
			 		</div>
		 		</>
		 	)
		 	:
		 	(
		 		<h1>No items in cart</h1>
		 	)
    
  );
}