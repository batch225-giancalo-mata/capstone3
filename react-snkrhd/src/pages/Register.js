import {Form, Button, Container} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register() {
	// Activity
	const {user, setUser} = useContext(UserContext)
	const navigate = useNavigate()
	// Activity END


	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	// For determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)


	function registerUser(event){
		event.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/signUp`, {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNumber,
						email:email,
						password: password1
					})
				})
				.then(response => response.json())
				.then(result => {
					console.log(result['message']);
					
					setEmail('');
					setPassword1('');
					setPassword2('');
					setFirstName('');
					setLastName('');
					setMobileNumber('');

					if(result['message'] != 'User already exists. Please login.'){
						Swal.fire({
							title: 'Register Successful!',
							icon: 'success',
							text: 'Welcome Sneakerhead!'
							})
						
						navigate('/login')

					} else {
						Swal.fire({	
							title: 'Registration Failed',
							icon: 'error',
							text: result["message"]
						})
					}		
				})

	}
	

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2])

	return (
		(user.id !==null) ?
			<Navigate to="/courses"/>
		:
		<Container style={{paddingTop: '50px'}}>
			<Form onSubmit={event => registerUser(event)}>

				<Form.Group controlId="firstName">
			        <Form.Label>First Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter First Name"
				            value={firstName} 
				            onChange={event => setFirstName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="lastName">
			        <Form.Label>Last Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Last Name"
				            value={lastName} 
				            onChange={event => setLastName(event.target.value)}
				            required
				        />
			    </Form.Group>

			    <Form.Group controlId="mobileNumber">
			        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control 
				            type="text" 
				            placeholder="Enter Mobile Number"
				            value={mobileNumber} 
				            onChange={event => setMobileNumber(event.target.value)}
				            required
				        />
			    </Form.Group>

			   	<Form.Group controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				            type="email" 
				            placeholder="Enter email"
				            value={email} 
				            onChange={event => setEmail(event.target.value)}
				            required
				        />
			        <Form.Text className="text-muted">
			            We'll never share your email with anyone else.
			        </Form.Text>
			    </Form.Group>

	            <Form.Group controlId="password1">
	                <Form.Label>Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Password" 
		                value={password1} 
				        onChange={event => setPassword1(event.target.value)}
		                required
	                />
	            </Form.Group>

	            <Form.Group controlId="password2">
	                <Form.Label>Verify Password</Form.Label>
	                <Form.Control 
		                type="password" 
		                placeholder="Verify Password" 
		                value={password2} 
				        onChange={event => setPassword2(event.target.value)}
		                required
	                />
	            </Form.Group>
	            <br/>

	            {	isActive ? 
	            		<Button variant="primary" type="submit" id="submitBtn">
			            	Submit
			            </Button>
			            :
			            <Button variant="primary" type="submit" id="submitBtn" disabled>
			            	Submit
			            </Button>
	            }
	            
			</Form>
		</Container>
	)
}