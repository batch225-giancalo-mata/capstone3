import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import NewArrivals from '../components/NewArrivals';
import { Container } from 'react-bootstrap';
import Footer from '../components/Footer';

export default function Home() {
  return (
    <>
	   	<div className="home-page">
	        <Banner />
	        <Container>
	        <div className="newarrivals-page">
	            <NewArrivals />
	     
	        </div>
	        	<div className="highlights-page">
	            <Highlights />
	        </div>
	        </Container>
	        <Footer />
	    </div>

    </>

  );
}
