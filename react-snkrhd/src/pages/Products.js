import ProductCard from '../components/ProductCard';
import Loading from '../components/Loading';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import AdminProducts from '../components/AdminProducts';
import AddProduct from '../components/AddProduct';

export default function Products() {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { user } = useContext(UserContext);

  useEffect((isLoading) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/inStock`)
      .then((response) => response.json())
      .then((result) => {
        console.log(result, "result")
        setProducts(
          result.map((product) => {
            return <ProductCard key={product._id} product={product} />;
          })
        );
        setIsLoading(false);
      });
  }, []);

  return (
    <>
      {isLoading ? (
        <Loading />
      ) : user.isAdmin ? (
        <>
          <AdminProducts />
        </>
      ) : (
        <>
          {products}
        </>
      )}
    </>
  );
}
