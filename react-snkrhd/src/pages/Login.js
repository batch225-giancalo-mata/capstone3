import { useState, useEffect, useContext } from 'react'
import { Form, Button, Card, Container, Row, Col } from 'react-bootstrap'
import { useNavigate, Navigate, Link } from 'react-router-dom'
import { decodeToken } from 'react-jwt'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login() {
  // Initializes the use of the properties from the UserProvider in App.js file
  const { user, setUser } = useContext(UserContext)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  // Initialize useNavigate
  const navigate = useNavigate()

  // For determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)
    
    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/getUser/${token}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(result => {


            // Store the user details retrieved from the token into the global user state

        })
    }

    function authenticate(event){
        event.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(result => {
            if(typeof result.access !== "undefined"){
                localStorage.setItem('token', result.access)

        const decode = decodeToken(result.access)


            setUser({
                id: decode.id,
                isAdmin: decode.isAdmin,
                jwt: result.access
            })               

                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: result.message
                })
           
           navigate("/products")

            } else {
                Swal.fire({
                    title: 'Authentication Failed!',
                    icon: 'error',
                    text: result.message
                })
            }
        })
    }

  useEffect(() => {
    if ((email !== '' && password !== '')) {
      // Enables the submit button if the form data has been verified
      setIsActive(true)
    } else {
      setIsActive(false)
    }
  }, [email, password])

  return (
    (user.id !== null) ?
      
      <Navigate to="/products" />
      :
      <div className= "login-page">
          <Card className= "login-card">
            <Card.Body>
            <h1>Sign In</h1>
            <br/>
                <Form onSubmit={event => authenticate(event)}>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={event => setEmail(event.target.value)}
                        required
                    />

                <br/>
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Password"
                        value={password}
                        onChange={event => setPassword(event.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                    Forgot password?
                    </Form.Text>
                </Form.Group>
                <br/>

                     {isActive ? (
                <Button
                  variant='primary'
                  type='submit'
                  className='submit-btn'
                  id='submitBtn'
                >
                  Submit
                </Button>
            ) : (
                <Button
                  variant='primary'
                  type='submit'
                  className='submit-btn'
                  id='submitBtn'
                  disabled
                >
                  Submit
                </Button>

            )}

            </Form>
            <br/>
            <p>Don't have an account? <Link to="/register">Register here </Link></p>
        </Card.Body>
    </Card>
        <div md={6} xs={0} className="tagline">
            <h1>WELCOME BACK SNKRHD!</h1>
            <h2>Exclusive offers await!</h2>
        </div>
     </div>
    );
}