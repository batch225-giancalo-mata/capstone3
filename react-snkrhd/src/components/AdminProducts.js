import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardImg } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import UpdateProduct from './UpdateProduct';
import Loading from '../components/Loading'
import UserContext from '../UserContext';
import AddProduct from './AddProduct';
export default function AdminProducts() {
	const { user } = useContext(UserContext)
	const [loading, setLoading] = useState(true)
	const [products, setProducts] = useState([])

	
	const getProducts = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/getAllProducts`, {
		  	headers: {
                Authorization: `Bearer ${user.jwt}`
            }
		})
		.then(response => response.json())
		.then(result => {
			console.log(result, "result")
			setProducts(result)
			setLoading(false)
		})
	}
	
	useEffect(() => {
		getProducts()

	}, [])


	return(
			
		(loading) ?
			
			<Loading/>
		: 
		 	<div>
		 		<AddProduct getProducts={getProducts} />
				{products.map((item) => (
					<Card key={item._id}>
				      <Card.Body>
				        <Card.Title>{item.name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{item.description}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>PHP {item.price}</Card.Text>
				       
				       <Card.Subtitle>Stock:</Card.Subtitle>
				       {item?.inStock ? (
				        	 <Card.Text>In stock</Card.Text>
				       	) : (
			       		 	<Card.Text>Out of stock</Card.Text>
				       	)}
				        <UpdateProduct product={item} getProducts={getProducts} />
				      </Card.Body>
				    </Card>
				))}
			</div>
	)
		
}
