import React, { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate, Link} from 'react-router-dom'
import { Modal, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

function UpdateProduct({ product, getProducts }) {
  const { user } = useContext(UserContext)
  const navigate = useNavigate()
  const [show, setShow] = useState(false);
  
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [name, setName] = useState(product?.name);
  const [price, setPrice] = useState(product?.price);
  const [inStock, setInStock] = useState(product?.inStock);
  const [description, setDescription] = useState(product?.description);
  const handleNameChange = event => {
    setName(event.target.value);
  };
  const handlePriceChange = event => {
    setPrice(event.target.value);
  };

  const handleStockChange = event => {
    const checked = event.target.checked
    setInStock(event.target.checked);

    if(checked) {
      handleUnarchive()
    } else {
      handleArchive()
    }

  };
  const handleUnarchive = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/unarchiveProduct/${product['_id']}`, {
      method: 'PUT',
      headers: {
        'Authorization': `Bearer ${user.jwt}`
      }
    })
    .then(response => response.json())
    .catch(error => {
      console.error(error);
      // show an error message
      Swal.fire({
        title: 'Error!',
        icon: 'error',
        text: 'Failed to unarchive the product.'
      });
    });
  };

   const handleArchive = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/archiveProduct/${product['_id']}`, {
      method: 'PUT',
      headers: {
        'Authorization': `Bearer ${user.jwt}`
      }
    })
    .then(response => response.json())
    .catch(error => {
      console.error(error);
      // show an error message
      Swal.fire({
        title: 'Error!',
        icon: 'error',
        text: 'Failed to archive the product.'
      });
    });
  };

  const handleDescriptionChange = event => {
    setDescription(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/updateProduct/${product['_id']}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.jwt}`
      },
      body: JSON.stringify({
        name: name,
        price: price,
        description: description,
        inStock: inStock
      })
    })
      .then(response => response.json())
      .then(result => {
        handleClose();
        Swal.fire({
          title: 'Update Successful!',
          icon: 'success',
          text: result.message
        })
      getProducts()
      });
  };

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Update 
      </Button>

      <Modal show={show} onHide={handleClose}>
        <form onSubmit={handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>Update Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <label>
              Product Name:
              <input type="text" value={name} onChange={handleNameChange} />
            </label>
            <br />

            <label>
              Product Price:
              <input type="text" value={price} onChange={handlePriceChange} />
            </label>
            <br />

            <label>
              Product Description:
              <input type="text" value={description} onChange={handleDescriptionChange} />
            </label>
            <br />

            <label>

                In Stock:
                <input type="checkbox" checked={inStock} onChange={handleStockChange} />
                </label>

            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button type="submit" variant="primary"> 
                Submit
              </Button>
            </Modal.Footer>
          </form>
      </Modal>
    </>
  );
}

export default UpdateProduct;
