import React, { useState, useEffect, useContext } from 'react';
import { Container, Table, Button } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function CartContents() {
  const { user } = useContext(UserContext);
  const [cart, setCart] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/cartContents`, {
      headers: {
        'Authorization': `Bearer ${user.jwt}`
      }
    })
    .then(response => response.json())
    .then(data => setCart(data.cart))
    .catch(error => console.error(error));
  }, [user.jwt]);

  const handleCheckout = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.jwt}`
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);
      setCart([]);
    })
    .catch(error => console.error(error));
  };

  return (
    <Container>
      <h1>Cart Contents</h1>
      {cart.length > 0 ? (
        <>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Product</th>
                <th>Quantity</th>
                <th>Total Price</th>
              </tr>
            </thead>
            <tbody>
              {cart.map(item => (
                <tr key={item._id}>
                  <td>{item.productId.name}</td>
                  <td>{item.quantity}</td>
                  <td>PHP {item.totalPrice.toFixed(2)}</td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Button onClick={handleCheckout}>Checkout</Button>
        </>
      ) : (
        <p>Your cart is empty.</p>
      )}
    </Container>
  );
}
