import { Row, Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function NewArrivals() {

    return (

        <Row className="mb-5">
        <h1 className="mt-5 new-arrivals">New Arrivals</h1>
        <h3 className="mb-3 pb-3">Check out our hottest kicks fresh from the oven.</h3>
            <Col md={3} sm={12}>
                <Link to="/products">
                    <Card className="cardNew1 p-3">
                        <Card.Body>
                            <Card.Title>
                                {/*<h2>1</h2>*/}
                            </Card.Title>

                        </Card.Body>
                    </Card>
                </Link>
            </Col>
            <Col md={3} sm={12}>
                <Link to="/products">
                    <Card className="cardNew2 p-3">
                        <Card.Body>
                            <Card.Title>
                                {/*<h2>2</h2>*/}
                            </Card.Title>
  
                        </Card.Body>
                    </Card>
                </Link>
            </Col>
            <Col md={3} sm={12}>
                <Link to="/products">
                    <Card className="cardNew3 p-3">
                        <Card.Body>
                            <Card.Title>
                                {/*<h2>3</h2>*/}
                            </Card.Title>
      
                        </Card.Body>
                    </Card>
                </Link>
            </Col>
            <Col md={3} sm={12}>
                <Link to="/products">
                    <Card className="cardNew4 p-3">
                        <Card.Body>
                            <Card.Title>
                                {/*<h2>4</h2>*/}
                            </Card.Title>

                        </Card.Body>
                    </Card>
                </Link>
            </Col>
        </Row>

    )
}
