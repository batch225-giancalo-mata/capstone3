import { Row, Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom';

export default function Highlights() {

    return (

        <Row className="mb-5">
        <h1 className="mt-5 shop-by-brand">Shop By Brand</h1>
        <h3 className="mb-3 pb-3">Get exclusive offers from the worlds top sports brands.</h3>
            <Col md={4} sm={12}>
                <Link to="/nike">
                    <Card className="cardHighlight1 p-3">
                        <Card.Body>
                            <Card.Title>
                                <h2>Shop Nike</h2>
                            </Card.Title>
                            <Card.Text>
                                Get exclusive access to limited editions.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Link>
            </Col>
            <Col md={4} sm={12}>
                <Link to="/adidas">
                    <Card className="cardHighlight2 p-3">
                        <Card.Body>
                            <Card.Title>
                                <h2>Shop Adidas</h2>
                            </Card.Title>
                            <Card.Text>
                                Get the best deals on Adidas NMDs.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Link>
            </Col>
            <Col md={4} sm={12}>
                <Link to="/puma">
                    <Card className="cardHighlight3 p-3">
                        <Card.Body>
                            <Card.Title>
                                <h2>Shop Puma</h2>
                            </Card.Title>
                            <Card.Text>
                                Get exclusive access to the fastest running shoes in the world. 
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Link>
            </Col>
        </Row>

    )
}
