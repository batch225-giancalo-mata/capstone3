import { Button, Row, Col } from 'react-bootstrap';
import React from 'react';
import '../App.css';

export default function Banner() {
  return (
    <Row>
      <Col className="tagline">
        <h1>LEGENDARY</h1>
        <h2>KICKS FOR TRUE</h2>
        <h1>SNKRHDS</h1>
    
        <div>
            <Button variant="primary" size="lg" className="shop-now-btn">SHOP NOW</Button>
        </div>  
      </Col>
      

    </Row>
  );
}