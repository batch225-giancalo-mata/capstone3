import React, { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { Modal, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

function AddProduct({getProducts}) {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [inStock, setInStock] = useState(true);
  const [description, setDescription] = useState('');

  const handleNameChange = event => {
    setName(event.target.value);
  };

  const handlePriceChange = event => {
    setPrice(event.target.value);
  };

  const handleStockChange = event => {
    setInStock(event.target.checked);
  };

  const handleDescriptionChange = event => {
    setDescription(event.target.value);
  };

  const handleSubmit = event => {
    event.preventDefault();
    console.log("form submitted")
    fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${user.jwt}`
      },
      body: JSON.stringify({
        name: name,
        description: description ? description : "none",
        size : "Mens 10US / 11UK",
        quantity : 10,
        price: price,
        inStock: inStock
      })
    })
    .then(response => response.json())
    .then(result => {
      handleClose();
      Swal.fire({
        title: 'Update Successful!',
        icon: 'success',
        text: result.message
      })
      getProducts()
    }).catch(error =>console.log(error, "Err"));
  };
    

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Add Product
      </Button>

      <Modal show={show} onHide={handleClose}>
        <form onSubmit={handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>Add Product</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <label>
              Product Name:
              <input type="text" value={name} onChange={handleNameChange} />
            </label>
            <br />

            <label>
              Product Description:
              <textarea value={description} onChange={handleDescriptionChange} />
            </label>
            <br />

            <label>
              Product Price:
              <input type="text" value={price} onChange={handlePriceChange} />
            </label>
            <br />

            <label>
              In Stock:
              <input type="checkbox" checked={inStock} onChange={handleStockChange} />
            </label>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button type="submit" variant="primary">
              Submit
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    </>
  );
}

export default AddProduct;
