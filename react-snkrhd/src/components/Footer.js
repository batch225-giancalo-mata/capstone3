import { Container } from 'react-bootstrap';
import { Row, Col, Card } from 'react-bootstrap';
import '../App.css';
import fbIcon from '../assets/fb-icon.svg';
import igIcon from '../assets/ig-icon.svg';
import twIcon from '../assets/tw-icon.svg';

export default function Footer() {
return (
<div  className="footer">
  <Container>
    <Row className="mt-5 p-2 align-items-center justify-content-center">
      <Col className="text-center">
        <Card.Body>
          <Card.Title>
            <p className="mt-5">Follow us</p>
            <a href="https://www.facebook.com/snkrhds">
            <img src={fbIcon} alt="Facebook" className="icon mx-3" />
            </a>
            <a href="https://www.instagram.com/snkrhds">
            <img src={igIcon} alt="Instagram" className="icon mx-3" />
            </a>
            <a href="https://www.twitter.com/snkrhds">
            <img src={twIcon} alt="Twitter" className="icon mx-3" />
            </a>
            <p className="mt-2">©2023 SNKRHD International | All Rights Reserved</p>
          </Card.Title>
        </Card.Body>
      </Col>
    </Row>
  </Container>
</div>
);
}
