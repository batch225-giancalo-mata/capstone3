import {Container, Row, Col, Card, CardImg} from 'react-bootstrap';
import React, { useState, useEffect, useContext } from 'react';
import {Link} from 'react-router-dom';
import { Modal, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import '../App.css';


export default function ProductCard({product}){
  const { user } = useContext(UserContext)
  // Destructuring the props
  const {name, description, price, image, _id} = product

  // Initialize a 'count' state with a value of zero (0)
  // const [count, setCount] = useState(0) 
  // const [slots, setSlots] = useState(15)
  // const [isOpen, setIsOpen] = useState(true)

  // console.log({name})
  // console.log({description})
  // console.log({_id})

  // function enroll(){
  //  if(slots > 0){
  //    setCount(count + 1)
  //    setSlots(slots - 1)

  //    return 
  //  }

  //  alert('Slots are full!')
  // }

  // Effects in React is just like side effects/effects in real life, where everytime something happens within the component, a function or condition runs. 
  // You may also listen or watch a specific state for changes instead of watching/listening to the whole component
  // useEffect(() => {
  //  if(slots === 0){
  //    setIsOpen(false)
  //  }
  // }, [slots])

 const addToCart = event => {
    // event.preventDefault();
    // console.log(`Product name: ${name}, Product id: ${product['_id']}`);
          fetch(`${process.env.REACT_APP_API_URL}/orders/addToCart`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${user.jwt}`
            },
            body: JSON.stringify({
                productId: _id,
                quantity: 1
            })
        })
        .then(response => response.json())
        .then(result => {
            
            // console.log(result)
            // handleClose()
       
                Swal.fire({
                    title: 'Add to Cart Successful!',
                    icon: 'success',
                    text: result.message
                })
           
           // navigate("/products")

        //     } else {
        //         Swal.fire({
        //             title: 'Authentication Failed!',
        //             icon: 'error',
        //             text: result.message
        //         })
        //     }
        }).catch(error => console.log(error, "error"))

  };

  return (
  <Container>
    <h1>{name}</h1>
    <Card>
      <div style={{ position: 'relative' }}>
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            height: '200px',
            backgroundColor: '#E8E8E8'
          }}
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              width: '50px',
              height: '50px',
              backgroundColor: '#FFFFFF',
              borderRadius: '5px'
            }}
          >
            <span style={{ fontSize: '30px' }}>+</span>
          </div>
        </div>
        <CardImg top src={image} />
      </div>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PHP {price}</Card.Text>
        <Button type="submit" variant="primary" onClick={addToCart}>
          Add To Cart
        </Button>
      </Card.Body>
    </Card>
  </Container>
)}

