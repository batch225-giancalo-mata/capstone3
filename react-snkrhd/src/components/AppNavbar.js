import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link, NavLink } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import logo from '../assets/snkrhd.svg';

import '../App.css';

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  console.log(user)
  return (
    <Navbar className="nav-bar" expand="lg">
      	<Navbar.Brand as={Link} to="/" className="navbar-brand">
        	<img src={logo} alt="snkrhd logo" />
      	</Navbar.Brand>
		
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
		
		<Navbar.Collapse id="basic-navbar-nav">
			<Nav className="ml-auto">
				<Nav.Link as={NavLink} to="/">Home</Nav.Link>
				<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
			
				{ (user.id) ?
					<>
						{!user.isAdmin && (
								<Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
						)}
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
					</>
					:
					<>
						<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
						<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
					</>
				}
				
			</Nav>
		</Navbar.Collapse>
	</Navbar>
)
}

